CREATE DATABASE facturacion;
USE facturacion;
SHOW DATABASES;
CREATE TABLE clientes(
id_cliente INT (2) 	AUTO_INCREMENT ,
nombre VARCHAR(60) NOT NULL,
apellidos VARCHAR (80),
direccion VARCHAR (200) NOT NULL,
facha_nac DATE,
telefono VARCHAR (10),
mail VARCHAR (100),
PRIMARY KEY (id_cliente) 
);


CREATE TABLE productos(
id_producto INT(3),
nombre_producto VARCHAR (60) NOT NULL,
precio DOUBLE,
stock INT,
PRIMARY KEY(id_producto)
);



CREATE TABLE facturas(
numero_factura INT AUTO_INCREMENT,
cliente INT (2),
fecha DATE,
PRIMARY KEY(numero_factura)
);


CREATE TABLE detalle(
numero_detalle INT AUTO_INCREMENT,
cantidad INT NOT NULL,
precio DOUBLE,
producto INT(3),
factura INT,
PRIMARY KEY (numero_detalle, factura)
);

ALTER TABLE facturas ADD COLUMN id_cliente INT (2);

ALTER TABLE facturas ADD CONSTRAINT fk_factura_cliente FOREIGN KEY (id_cliente)
REFERENCES  clientes(id_cliente)
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE detalle ADD CONSTRAINT fk_detalle__producto FOREIGN KEY (producto)
REFERENCES productos (id_producto)
ON DELETE RESTRICT ON UPDATE CASCADE;

ALTER TABLE detalle ADD CONSTRAINT fk_detalle_factura FOREIGN KEY (factura)
REFERENCES facturas (numero_factura)
ON DELETE RESTRICT ON UPDATE CASCADE;
